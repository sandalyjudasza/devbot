

#include <Servo.h> 

const int BUZZER_PIN = 5;
const int SERVO_PIN = 12;

Servo headServo;

void safeDelayUsec(unsigned int us) {
#if F_CPU >= 16000000L
      if (--us == 0)
            return;
      us <<= 2;
      us -= 2;
#else
      if (--us == 0)
            return;
      if (--us == 0)
            return;
      us <<= 1;
        us--;
#endif
      __asm__ __volatile__ (
            "1: sbiw %0,1" "\n\t" // 2 cycles
            "brne 1b" : "=w" (us) : "0" (us) // 2 cycles
      );
}

void safeDelayMsec(long int milliseconds) {
  for (long int i=0; i<milliseconds; i++) {
    safeDelayUsec(1000);
  }
}

void setHeadAngle(int angle, int frict = 25) {
  static int headAngle = 90;
  if (angle == headAngle) {
    return;
  }
  if (angle > headAngle) {
    for (; headAngle < angle; headAngle++) {
      headServo.write(headAngle);
      safeDelayMsec(frict);
    }
  }
  else {
    for (; headAngle > angle; headAngle--) {
      headServo.write(headAngle);
      safeDelayMsec(frict);
    }
  }
  headAngle = angle;
  headServo.write(headAngle);
  safeDelayMsec(frict);
}

void safeTone(int frequency, int time) {
  long int microseconds = 1000000L / (frequency * 2);
  long int count = (long int)frequency * time / 1000;
  for (int i=0; i<count; i++) {
    digitalWrite(BUZZER_PIN, HIGH);
    safeDelayUsec(microseconds);
    digitalWrite(BUZZER_PIN, LOW);
    safeDelayUsec(microseconds);
  }
}

void sing() {
  setHeadAngle(random(20, 160), 5);
  safeDelayMsec(random(200, 200));

  int notes = random(6, 20);
  for (int i=0; i<notes; i++) {
    safeTone(random(100, 2000), random(10,100));
  }
  safeTone(random(100, 2000), random(100,200));

  safeDelayMsec(random(200, 200));
}

void grumble() {
  setHeadAngle(random(50, 130), 35);
  safeDelayMsec(random(550, 900));
  safeTone(16, random(200, 1200));
  safeDelayMsec(random(550, 900));
}

int degreesToADCscale(float degr) {
  float mVolts = 500.0 + 10.0 * degr;
  return (int)(mVolts * 1024.0 / 5000.0);
}

void lightProgress(int stage) {
  int counter = 0;
  for (int pin=53; pin>=39; pin-=2) {
    digitalWrite(pin, (counter <= stage) ? HIGH : LOW);
    counter++;
  }
}

float weightedInput = 500 + 20*10; // 20 degC
float tempDiffThreshold = degreesToADCscale(26) - degreesToADCscale(24);  // 2 degC difference

float lastLowestTemperature = weightedInput;
float lastHighestTemperature = weightedInput;

enum State {
  GRUMBLE,
  SING
} botState = GRUMBLE;

void setup() {
  for (int i=39; i<=53; i+=2) {
    pinMode(i, OUTPUT);
  }
  pinMode(BUZZER_PIN, OUTPUT);
  headServo.attach(SERVO_PIN);
  headServo.write(90);
  setHeadAngle(90);

  weightedInput = analogRead(0);
  lastLowestTemperature = weightedInput;
  lastHighestTemperature = weightedInput;
}

void grumbleLoop() {
  grumble();
  if (weightedInput <= lastLowestTemperature) {
    lastLowestTemperature = weightedInput;
    return;
  }
  float tempDiff = weightedInput - lastLowestTemperature;
  if (tempDiff < tempDiffThreshold) {
    lightProgress(map(tempDiff, 0, tempDiffThreshold, 0, 8));
    return;
  }
  else {
    botState = SING;
    lastHighestTemperature = weightedInput;
    lightProgress(7);
  }
}

void singLoop() {
  sing();
  if (weightedInput >= lastHighestTemperature) {
    lastHighestTemperature = weightedInput;
    return;
  }
  float tempDiff = lastHighestTemperature - weightedInput;
  if (tempDiff < tempDiffThreshold) {
    lightProgress(7 - map(tempDiff, 0, tempDiffThreshold, 0, 8));
    return;
  }
  else {
    botState = GRUMBLE;
    lastLowestTemperature = weightedInput;
    lightProgress(0);
  }
}

//void loop() {
// sing();
// grumble();
//  safeTone(100, 1000);
//  safeDelayMsec(1000);
//}


void loop() {
  float input = analogRead(0);
  weightedInput = 0.9*weightedInput + 0.1*input;

  switch (botState) {
    case GRUMBLE:
      grumbleLoop();
      break;
    case SING:
      singLoop();
      break;
  }
}



